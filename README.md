1. Download all PDFs from EasyChair to `data/pdf`
2. Download `submission.csv` from EasyChair's all-data export page to `data/submission.csv`
3. Reorder lines of submission.csv to the order you want the papers to appear in the proceedings (possibly by track, then in presentation order)
5. in `data` directory, run `../easy2acl/easy2acl.py`, which should generate all the ACL Bibtex files and the proceedings skeleton
7. Edit `data/output/book-proceedings.tex` as you see fit
8. Run `pdflatex book-proceedings.tex`
9. Manually extract the Frontmatter portion (Roman numeral numbers) of the compiled Proceedings PDF and copy to `data/pdf/{prefix}_frontmatter.pdf`
10. Copy the entire book proceedings PDF to `data/pdf/{prefix}.pdf`. After this step all the data is in the locations expected by the standard ACLPUB ingestion scripts.
11. Run `../easy2acl/easy2acl.py` again
12. Once this data is generated, you can proceed with [the ingestion scripts for the ACL Anthology](https://github.com/acl-org/ACLPUB) to generate the XML ingestion file and layout that the Anthology requires. Run `make-anthology.sh` from the `output` directory

This is adapted from [https://github.com/acl-org/easy2acl](the easychair ACL scripts) but has been modified a lot
