#!/usr/bin/env python3

#
# easy2acl.py - Convert data from EasyChair for use with ACLPUB
#
# Original Author: Nils Blomqvist
# Forked/modified by: Asad Sayeed
# Further modifications and docs (for 2019 Anthology): Matt Post
# Index for LaTeX book proceedings: Mehdi Ghanimifard and Simon Dobnik
#
# Please see the documentation in the README file at http://github.com/acl-org/easy2acl.

import os
import re
import sys
import argparse
from os import path
from csv import DictReader
from glob import glob
from shutil import copy, rmtree
from collections import namedtuple
from unicode_tex import unicode_to_tex
from pybtex.database import BibliographyData, Entry
from PyPDF2 import PdfFileReader


AnthologyInfo = namedtuple('AnthologyInfo', ['collection', 'year', 'volume', 'paper_width'])


def texify(string):
    """Return a modified version of the argument string where non-ASCII symbols have
    been converted into LaTeX escape codes.

    """
    return ' '.join(map(unicode_to_tex, string.split())).replace(r'\textquotesingle', "'")


#,----
#| Metadata
#`----

def read_meta(filename):
    metadata = { 'chairs': [] }
    with open('meta') as metadata_file:
        for line in metadata_file:
            key, value = line.rstrip().split(maxsplit=1)
            if key == 'chairs':
                metadata[key].append(value)
            else:
                metadata[key] = value

    for key in 'abbrev title booktitle month year location publisher chairs bib_url'.split():
        if key not in metadata:
            print('Fatal: missing key "{}" from "meta" file'.format(key))
            sys.exit(1)

    match = re.match(r'https://www.aclweb.org/anthology/([A-Z])(\d\d)-(\d+)%0(\d+)d', metadata['bib_url'])
    if match is None:
        print("Fatal: bib_url field ({}) in 'meta' file has wrong pattern".format(metadata['bib_url']), file=sys.stderr)
        sys.exit(1)
    anthology_info = AnthologyInfo(*match.groups())
    return metadata, anthology_info

Submission = namedtuple('Submission', ['id', 'authors', 'abstract', 'title', 'track', 'decision'])

track_names = {
    'long_oral': 'Long Papers',
    'short_oral': 'Short Papers',
    'long_poster': 'Long Papers (Posters)',
    'short_poster': 'Short Papers (Posters)',
    'shared_task': 'Shared Task (Not Peer Reviewed)'
}

def track_key_from_submission(submission):
    if submission.track == 1:
        if submission.decision == 'probably accept':
            return 'long_poster'
        elif submission.decision == 'accept':
            return 'long_oral'
    elif submission.track == 2:
        if submission.decision == 'probably accept':
            return 'short_poster'
        elif submission.decision == 'accept':
            return 'short_oral'
    elif submission.track == 4:
        return 'shared_task'


def submission_from_row(row):
    return Submission(
        id=int(row['#']),
        authors=re.split(r'(?:, | and )', row['authors']),
        title=row['title'],
        abstract=row['abstract'],
        track=int(row['track #']),
        decision=row['decision']
    )

def build_proceedings(input_dir='.', output_dir='./output', submission_csv=None, include_submission=None):
    metadata, anth_info = read_meta(f'{input_dir}/meta')
    if not path.exists(output_dir):
        os.makedirs(output_dir, exist_ok=True)

    submissions = {}
    
    if submission_csv is None:
        submission_csv = f'{input_dir}/submission.csv'

    with open(submission_csv) as f:
        reader = DictReader(f)
        all_submissions = [submission_from_row(row) for row in reader]

    include_submission = (lambda x: True) if include_submission is None else include_submission

    included = [s for s in all_submissions if include_submission(s)]
    submissions = {s.id: s for s in included}

    abbrev = metadata['abbrev']
    year = metadata['year']
    prefix = f'{abbrev}{year}'

    proceedings_dir = f'{output_dir}/data/{prefix}/proceedings'
    if not path.exists(proceedings_dir):
        os.makedirs(proceedings_dir, exist_ok=True)
    proceedings_data_dir = f'{proceedings_dir}/cdrom'

    #
    # Find all relevant PDFs
    #

    input_pdf_dir = f'{input_dir}/pdf'
    # The PDF of the full proceedings
    full_pdf_file = f'{input_pdf_dir}/{prefix.lower()}.pdf'

    if not os.path.exists(full_pdf_file):
        print(f"Creating empty find full volume PDF '{full_pdf_file}'")
        with open(full_pdf_file, 'w') as _:
            pass

    # The PDF of the frontmatter
    frontmatter_pdf_file = f'{input_pdf_dir}/{prefix}_frontmatter.pdf'

    if not os.path.exists(frontmatter_pdf_file):
        print(f"Creating empty frontmatter PDF file '{frontmatter_pdf_file}'")
        with open(frontmatter_pdf_file, 'w') as _:
            pass

    # File locations of all PDFs (seeded with PDF for frontmatter)
    pdfs = { 0: frontmatter_pdf_file }
    for pdf_file in glob(f'{input_pdf_dir}/{prefix}_paper_*.pdf'):
        submission_id = int(pdf_file.split('_')[-1].replace('.pdf', ''))
        pdfs[submission_id] = pdf_file

    # List of accepted papers (seeded with frontmatter)
    included.insert(0, Submission(0, metadata['chairs'], metadata['booktitle'], '', 0, 'accept'))

    #
    # Create Anthology tarball
    #

    # Create destination directories
    for dir in ['bib', 'pdf']:
        dest_dir = os.path.join(f'{proceedings_data_dir}', dir)
        os.makedirs(dest_dir, exist_ok=True)

    # Copy over "meta" file
    print(f'COPYING meta -> {proceedings_dir}/meta', file=sys.stderr)
    copy('meta', f'{proceedings_dir}/meta')

    anth_pdf_dir = f'{proceedings_data_dir}/pdf'
    os.makedirs(anth_pdf_dir, exist_ok=True)

    anthology_paths = {}
    final_bibs = []
    start_page = 1
    for paper_id, submission in enumerate(included):
        submission_id = submission.id
        if not submission_id in pdfs:
            print('Fatal: no PDF found for paper', paper_id, file=sys.stderr)
            sys.exit(1)

        pdf_path = pdfs[submission_id]
        formatted_id = '{paper_id:0{width}d}'.format(paper_id=paper_id, width=anth_info.paper_width)
        dest_path = f'{anth_pdf_dir}/{anth_info.collection}{anth_info.year}-{anth_info.volume}{formatted_id}.pdf'
        anthology_paths[submission_id] = dest_path

        copy(pdf_path, dest_path)
        print('COPYING', pdf_path, '->', dest_path, file=sys.stderr)

        bib_path = dest_path.replace('pdf', 'bib')
        if not os.path.exists(os.path.dirname(bib_path)):
            os.makedirs(os.path.dirname(bib_path))

        anthology_id = os.path.basename(dest_path).replace('.pdf', '')

        bib_type = 'inproceedings' if submission_id != '0' else 'proceedings'
        bib_entry = Entry(bib_type, [
            ('author', texify(' and '.join(submission.authors))),
            ('title', submission.title),
            ('year', metadata['year']),
            ('month', metadata['month']),
            ('address', metadata['location']),
            ('publisher', metadata['publisher']),
            ('abstract', submission.abstract)
        ])

        # Add page range if not frontmatter
        if paper_id > 0:
            with open(pdf_path, 'rb') as in_:
                file = PdfFileReader(in_)
                last_page = start_page + file.getNumPages() - 1
                bib_entry.fields['pages'] = '{}--{}'.format(start_page, last_page)
                start_page = last_page + 1

        # Add booktitle for non-proceedings entries
        if bib_type == 'inproceedings':
            bib_entry.fields['booktitle'] = metadata['booktitle']

        try:
            bib_string = BibliographyData({anthology_id: bib_entry}).to_string('bibtex')
        except TypeError as e:
            print('Fatal: Error in BibTeX-encoding paper', submission_id, file=sys.stderr)
            sys.exit(1)
        final_bibs.append(bib_string)
        with open(bib_path, 'w') as out_bib:
            print(bib_string, file=out_bib)
            print('CREATED', bib_path)


    book_proceedings = f'{output_dir}/book-proceedings'
    # Create an index for LaTeX book proceedings
    os.makedirs(book_proceedings, exist_ok=True)
    rmtree(f'{book_proceedings}/pdf')
    os.makedirs(f'{book_proceedings}/pdf', exist_ok=True)

    latex_by_track = {}
    with open(f'{book_proceedings}/all_papers.tex', 'w') as f:
        for track_key, track_name in track_names.items():
            print(f"Writing out track {track_name} to book proceedings")
            print('\\addtocontents{toc}{{}\\\\[10pt] \\textbf{%s}\\vspace{5pt}}' % track_name, file=f)
            for entry in included:
                # submission_id, paper_title, authors = entry
                submission_id = entry.id
                track_key_for_entry = track_key_from_submission(entry)
                print(f"Processing submission {submission_id} in track {track_key_for_entry}")
                if track_key_for_entry != track_key or submission_id == 0:
                    continue
                if len(entry.authors) > 1:
                    authors = ', '.join(entry.authors[:-1]) + ' and ' + entry.authors[-1]
                else:
                    authors = entry.authors[0]
                pdf_for_submission = pdfs[submission_id]
                target_pdf_path = f'{book_proceedings}/{pdf_for_submission}'
                print(f'COPYING {pdf_for_submission} -> {target_pdf_path}')
                copy(pdf_for_submission, target_pdf_path)
                line = f"""\goodpaper{{{pdf_for_submission}}}{{{texify(entry.title)}}}%
            {{{texify(authors)}}}\n"""
                print(line, file=f)
    

    # Write the volume-level bib with all the entries
    dest_bib = f"{proceedings_data_dir}/{metadata['abbrev']}-{metadata['year']}.bib"

    with open(dest_bib, 'w') as whole_bib:
        print('\n'.join(final_bibs), file=whole_bib)
        print('CREATED', dest_bib)

    # Copy over the volume-level PDF
    dest_pdf = dest_bib.replace('bib', 'pdf')
    print('COPYING', full_pdf_file, '->', dest_pdf, file=sys.stderr)
    copy(full_pdf_file, dest_pdf)


def main():
    parser = argparse.ArgumentParser()
    # parser.add_argument('-i', '--input-dir', default='.', help='path containing "meta" file, "pdf" dir and "submission.csv"')
    parser.add_argument('-o', '--output-dir', default='output', help='path to write output files for later import into ACL anthology')
    parser.add_argument('--submission-csv', default=None, help='path to submission.csv if not in input dir')
    # parser.add_argument('--tracks-to-include', default=None, help='track numbers to include in proceedings (default: all)')

    args = parser.parse_args()

    # submission_filter = []
    # if args.tracks_to_include is not None:
    #     tracks_to_include = args.tracks_to_include.split(',')
    #     print(f"Only including tracks {tracks_to_include}")
    #     submission_filter = lambda x: tracks_to_include.contains(x)
    def include_submission(submission):
        return (submission.track in (1, 2) and submission.decision.lower().endswith('accept')) or submission.track == 4

    build_proceedings('.', args.output_dir, args.submission_csv, include_submission)

if __name__ == "__main__":
    main()